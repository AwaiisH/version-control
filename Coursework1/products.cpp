// ====================================================================
// Date : 20 Jan 2021
// Title: Products Definitions 
// MISIS: M00692020
// ====================================================================

#include "products.hpp"


// ====================================================================
// static variables 
map<string, Product*> Product::products;
string Product::save_file_path = "save.txt";


// ====================================================================
Product::Product() : n_items(0), n_sales(0)
{
    
}


// ====================================================================
Product::~Product() 
{
    // do nothing 
}


// ====================================================================
void Product::restock(int amount)
{
    // check valid and restock
    if (amount > 0)
        n_items += amount;
}


// ====================================================================
bool Product::sell(int amount)
{   
    // check if possible
    if (n_items < amount)
        return false;
    
    // update items and sales
    n_items -= amount;
    n_sales += amount;

    return true;
}


// ====================================================================
void Product::updateStock(int amount)
{
    if (amount > 0)
        n_items = amount;
}


// ====================================================================
void Product::setNSales(int sales)
{
    n_sales = sales;
}


// ====================================================================
int Product::getNSales()
{
    return n_sales;
}

int Product::getNItems()
{
    return n_items;
}

// ====================================================================
void Product::clear()
{
    // delete each item
    for(auto it = products.begin(); it != products.end(); it++)
        delete (*it).second;

    products.clear();
}

void Product::showReport()
{
    int total = 0;

    cout << endl << "Sales Report------------" << endl;

    // go through each product 
    for(auto it = products.begin(); it != products.end(); it++)
    {
        Product* p = (*it).second;
        int price, n_sales;
        price = p->price;
        n_sales = p->getNSales();

        // calculate sales and print
        cout << "ID: " << p->id << ", UNIT PRICE: " << price << ", NO OF SALES: " <<
                n_sales << ", TOTAL: " << price * n_sales << endl;

        // total 
        total += price * n_sales;
    }

    cout << "TOTAL INCOME FROM THE SALES = " << total << endl;
}

// ====================================================================
Product* Product::get(string id)
{
    auto it = products.find(id);

    // find the product and return 
    if (it == products.end())
    {   
        return NULL;
    }

    return (*it).second;
}


// ====================================================================
bool Product::remove(string id)
{
    Product* product;

    if ((product = Product::get(id)) != NULL)
    {
        delete product;
        products.erase(id);
        return true;
    }

    return false;
}


// ====================================================================
void Product::save() 
{
    // open the file stream
    ofstream fs;
    fs.open(save_file_path);
    fs.clear();

    // go through each product 
    for (auto it = products.begin(); it != products.end(); it++)
    {   
        // get the save string 
        string s = (*it).second->getSaveString();
        fs << s << endl;
    }
    fs.close();
}


// ====================================================================
int Product::getIntLine(ifstream& fs)
{
    string s;
    getline(fs, s); // get the line as string 
    stringstream ss(s); // add to a sstream
    int i;  
    ss >> i;    // convert to int
    return i;
}


// ====================================================================
void Product::load() 
{
    products.clear();
    
    // open file
    ifstream fs;
    fs.open(save_file_path, ios::out);

    string s;

    // load each product type
    while(getline(fs, s))
    { 
        int type;
        stringstream ss(s);
        ss >> type; // first line is the type
        
        string id;
        int price, n_items, n_sales;

        getline(fs, id);    // the id
        price = getIntLine(fs); // next is price
        n_items = getIntLine(fs);   // stock
        n_sales = getIntLine(fs);   // sales

        string s1, s2, s3;
        int i;
        Product* p;

        switch (type)
        {
        case 1:
            getline(fs, s1);    // name 
            getline(fs, s2);    // author
            p = new Book(id, price, s1, s2);
            break;

        case 2:
            getline(fs, s1);    // album
            getline(fs, s2);    // genre
            getline(fs, s3);    // artist
            p = new CD(id, price, s1, s2, s3);
            break;

        case 3:
            getline(fs, s1);    // name
            i = getIntLine(fs); // year
            getline(fs, s2);    // producer
            getline(fs, s3);    // genre
            p = new DVD(id, price, s1, i, s2, s3);
            break;

        case 4:
            getline(fs, s1);    // author
            getline(fs, s2);    // publish date
            getline(fs, s3);    // topic
            p = new Magazine(id, price, s1, s2, s3); 
            break;

        default:
            break;
        }

        // update product list and items, sales
        if (type <= 4 && type >= 1)
        {
            products[id] = p;
            p->updateStock(n_items);
            p->setNSales(n_sales);
        }
    }

    fs.close();
}



// ====================================================================
Book::Book(string id_, int price_, string name_, string author_name_) :
     name(name_), author_name(author_name_)
{
    id = id_;
    price = price_;
    products[id] = this;
}

// ====================================================================
Book::~Book()
{

}


// ====================================================================
string Book::getSaveString()
{
    // converts properties to a string seperated by new lines 
    return "1\n" + id + "\n" + to_string(price) + "\n" + to_string(n_items) + "\n" +
             to_string(n_sales) + "\n" + name + "\n" + author_name;

}


// ====================================================================
CD::CD(string id_, int price_, string album_, string genre_, string artist_) : 
        album(album_), genre(genre_), artist(artist_) 
{
    id = id_;
    price = price_;
    products[id] = this;
}


// ====================================================================
CD::~CD()
{
    // do nothing 
}

// ====================================================================
string CD::getSaveString()
{
    // converts properties to a string seperated by new lines 
    return "2\n" + id + "\n" + to_string(price) + "\n" + to_string(n_items) + "\n" + 
            to_string(n_sales) + "\n" + album + "\n" + genre + "\n" + artist;
}



// ====================================================================
DVD::DVD(string id_, int price_, string name_, int year_released_, 
        string producer_, string genre_) : 
    name(name_), year_released(year_released_), 
    producer(producer_), genre(genre_)
{
    id = id_;
    price = price_;
    products[id] = this;
}


// ====================================================================
DVD::~DVD()
{
    // do nothing   
}

// ====================================================================
string DVD::getSaveString()
{
    // converts properties to a string seperated by new lines 
    return "3\n" + id + "\n" + to_string(price) + "\n" + to_string(n_items) + "\n" + 
            to_string(n_sales) + "\n" + name + "\n" + to_string(year_released) +
             "\n" + producer + "\n" + genre;
}


// ====================================================================
Magazine::Magazine(string id_, int price_, string author_name_, 
    string pubish_date_, string topic_) : 
    author_name(author_name_), publish_date(pubish_date_), topic(topic_)
{
    id = id_;
    price = price_;
    products[id] = this;
}   


// ====================================================================
Magazine::~Magazine()
{
    // do nothing 
}

// ====================================================================
string Magazine::getSaveString()
{
    // converts properties to a string seperated by new lines 
    return "4\n" + id + "\n" + to_string(price) + "\n" + to_string(n_items) + "\n" + 
            to_string(n_sales) + "\n" + author_name + "\n" + publish_date + 
            "\n" + topic;
}
