// ====================================================================
// Date : 20 Jan 2021
// Title: Products header 
// MISIS: M00692020
// ====================================================================

#ifndef _PRODUCTS
#define _PRODUCTS

#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

// ====================================================================
// Note : create product only useing dynamic allocation (using new)
class Product 
{
public:
    string id;  //id
    int price;  // price

    // the global array that stores all the products
    static map<string, Product*> products;


protected:
    int n_items;    // current stock
    int n_sales;    // no of total sales 

    static string save_file_path;   // path of the save file


public:
    Product();  // constructor

    virtual ~Product(); // destructor

    // member functions.    
    void restock(int amount);  
    bool sell(int amount);
    void updateStock(int amount);
    void setNSales(int sales);
    int getNSales();
    int getNItems();

    // return the objest as string to save
    virtual string getSaveString() = 0;

    // get the pointer to product for the given id
    // return NULL if not found
    static Product* get(string id);
    static bool remove(string id);  // dlete item
    static void save(); // save to file
    static void load(); // load from the file
    static void showReport();   // show the report

    static void clear();    // clean the memory

private:
    // get the integer from the line of file
    static int getIntLine(ifstream& fs);    
};


// ====================================================================
// the book class 
class Book : public Product
{
public:
    string name;
    string author_name;

public:
    Book(string id_, int price_, string name_, string author_name_);
    ~Book();

    string getSaveString();
};

// ====================================================================
// CD class
class CD : public Product
{
public:
    string album;
    string genre;
    string artist;

public:
    CD(string id_, int price_, string album_, string genre_, string artist_);
    ~CD();

    string getSaveString();
};

// ====================================================================
// DVD class
class DVD : public Product
{
public:
    string name;
    int year_released;
    string producer;
    string genre;

public:
    DVD(string id_, int price_, string name_, int year_released_, 
            string producer_, string genre_);
    ~DVD();

    string getSaveString();
};

// ====================================================================
// Magazine class
class Magazine : public Product 
{
public:
    string author_name;
    string publish_date;
    string topic;

public:
    Magazine(string id_, int price_, string author_name_, 
            string pubish_date_, string topic_);
    ~Magazine();

    string getSaveString();
}; 

#endif
