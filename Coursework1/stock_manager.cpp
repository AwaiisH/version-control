// ====================================================================
// Date : 20 Jan 2021
// Title: Stock Manager
// MISIS: M00692020
// ====================================================================

#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include "products.hpp"


using namespace std;


// the functions used show the main menu
int mainMenu();

// this functions is used to sell items acoording the 
// user inputs 
void sellProductMenu();

// this functions show the menu to restock options
void restockProductMenu();

// the function used to update the stock of a product
void updateStockProductMenu();

// this will show the menu to create any type or product
void addNewProductMenu();

// shows options to delete an item
void deleteProductMenu();

// those function used to add each type of products 
// wiil take different parameters as user inputs 
void addBookMenu(string id, int price);
void addCDMenu(string id, int price);
void addDVDMenu(string id, int price);
void addMagazineMenu(string id, int price);

// show the given error
void showError(string error);



// ====================================================================
int main()
{
    // quite when this is true
    bool quit = false;

    // load stocks form the file if available 
    Product::load();

    // run repeatedly 
    while (!quit)
    {
        int i = mainMenu(); // show main menu

        // check the user input menu item number
        switch (i)
        {
        case 1: // 1 to sell a product
            sellProductMenu();
            break;

        case 2: // to restock product
            restockProductMenu();
            break;

        case 3: // 3 to update stock 
            updateStockProductMenu();
            break;

        case 4: // 4 to add new product 
            addNewProductMenu();
            break;

        case 5: // 5 delete product 
            deleteProductMenu();
            break;

        case 6: // 6 to show the sales report
            Product::showReport();
            break;

        default:    // quite otherwise
            quit = true;
            break;
        }
    }

    Product::save();    // save products to a file

    Product::clear();   // clear the memory

    cout << "Quiting the program..." << endl;

    return 0;
}


// ====================================================================
int mainMenu()
{
    // show main menu
    cout << endl << "Main Menu----------" << endl;

    cout << "1 Sell product" << endl << "2 Restock item"
        << endl << "3 Update stock" << endl << "4 Add new product" << endl
        << "5 Delete product" << endl << "6 View report" << endl << 
        "7 (or any other) Quit" << endl;

    // get the menu item number 
    cout << endl << "Enter the number: ";

    int i;

    cin >> i;

    return i;
}



// ====================================================================
void sellProductMenu()
{
    cout << endl << "Sell Product--------------" << endl;

    // get product id from the user
    cout << "Enter product id: " << endl;
    string id;
    cin >> id;

    Product* product;

    // if product is available
    if ((product = Product::get(id)) != NULL)
    {
        // get the number of itesm to sell
        cout << "Enter amount: " << endl;   
        int amount;
        cin >> amount;

        // sell with error checking
        if (amount < 0 || !product->sell(amount))
        {
            showError("Sell amount is not valid or not enough stocks");
        }
        else 
            cout << "Sold " << amount << " of " << id << endl;
    }
    else 
        showError("Product not found");

}


// ====================================================================
void addNewProductMenu()
{
    cout << endl << "Add New Product------------" << endl;

    // add new product menu
    cout << "1 Book" << endl << "2 CD" << endl << "3 DVD" << endl 
        << "4 Magazine" << endl << "5 back" << endl;

    cout << endl << "Enter the number: ";

    int type;

    cin >> type;

    string id;
    int price;

    // get id and price form the user
    if (type >= 1 && type <=4 )
    {
        cout << "Enter the ID (Cannot contain spaces): " << endl;

        cin >> id;

        cout << "Enter the price: " << endl;

        cin >> price;
    }

    // price error
    if (price <= 0)
    {
        showError("Price is not valid");
        return;
    }

    // check the id is available 
    if (Product::get(id) == NULL)
    {
        // type
        switch (type)
        {
        case 1: // add book
            addBookMenu(id, price);
            break;

        case 2: // add cd
            addCDMenu(id, price);
            break;

        case 3: // add DVD
            addDVDMenu(id, price);
            break;

        case 4: // add magazine
            addMagazineMenu(id, price);
        
        default:
            break;
        }

    }
    else 
        showError("ID already exists");
}


// ====================================================================
void addBookMenu(string id, int price)
{
    string name, author;
    
    // get the name
    cout << "Enter name of the book:" << endl;
    cin.ignore();
    getline(cin, name);

    // get the author's name
    cout << "Enter Author's name of the book:" << endl;
    cin.ignore();
    getline(cin, author);

    new Book(id, price, name, author);
}

// ====================================================================
void addCDMenu(string id, int price)
{
    string album, genre, artist;

    // get the albnum
    cout << "Enter the album:" << endl;
    cin.ignore();
    getline(cin, album);

    // get the genre
    cout << "Enter the genre:" << endl;
    cin.ignore();
    getline(cin, genre);

    // get the artist name
    cout << "Enter the name of the artist:" << endl;
    cin.ignore();
    getline(cin, artist);

    new CD(id, price, album, genre, artist);
}


// ====================================================================
void addDVDMenu(string id, int price)
{
    string name, producer, genre;
    int year;

    // get the name
    cout << "Enter the name:" << endl;
    cin.ignore();
    getline(cin, name);

    // get the year
    cout << "Enter the year released:" << endl;
    cin >> year;

    // get the producer
    cout << "Enter the name of the producer:" << endl;
    cin.ignore();
    getline(cin, producer);

    // get the genre
    cout << "Enter the genre:" << endl;
    cin.ignore();
    getline(cin, genre);

    new DVD(id, price, name, year, producer, genre);
}


// ====================================================================
void addMagazineMenu(string id, int price) 
{
    string author, date, topic;

    // get the author's name
    cout << "Enter the author's name:" << endl;
    cin.ignore();
    getline(cin, author);

    // get the date 
    cout << "Enter the date of the magazine:" << endl;
    cin.ignore();
    getline(cin, date);

    // get the topoic
    cout << "Enter the topic of the magazien:" << endl;
    cin.ignore();
    getline(cin, topic);

    new Magazine(id, price, author, date, topic);

}


// ====================================================================
void restockProductMenu()
{
    cout << endl << "Restock Product--------------" << endl;

    // get the id 
    cout << "Enter product id: " << endl;
    string id;
    cin >> id;

    Product* product;

    // if the product is available 
    if ((product = Product::get(id)) != NULL)
    {
        cout << "Enter amount: " << endl;   
        int amount;
        cin >> amount;

        if (amount < 0)
        {
            showError("Restock amount is not valid");
        }
        else
            product->restock(amount);
    }
    else 
        showError("Product not found");
}


// ====================================================================
void updateStockProductMenu()
{
    cout << endl << "Update Stock of a Product--------" << endl;

    // get the id
    cout << "Enter product id: " << endl;

    string id;

    cin >> id;

    Product* product;

    if ((product = Product::get(id)) != NULL)
    {
        cout << "Enter amount: " << endl;   
        int amount;
        cin >> amount;

        // chekc the amount is valid
        if (amount < 0)
        {
            showError("New stock amount is not valid");
        }
        else
            product->updateStock(amount);
    }
    else 
        showError("Product not found");
}


// ====================================================================
void deleteProductMenu()
{
    cout << endl << "Delete a Product--------" << endl;

    // get the product id
    cout << "Enter product id: " << endl;

    string id;

    cin >> id;

    Product* product;

    // remove the item
    if (Product::remove(id))
        cout << "Product deleted: " << id << endl;

    else 
        showError("Product not found");

}

// ====================================================================
void showError(string error)
{
    // print error
    cout << "Error: " << error << endl;
}
