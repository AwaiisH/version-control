// ====================================================================
// Date : 20 Jan 2021
// Title: Test
// MISIS: M00692020
// ====================================================================

#define CATCH_CONFIG_MAIN  
#include "catch.hpp"
#include "products.hpp"


TEST_CASE( "stock manager test", "[product]" ) 
{
    // add 2 products 
    Product *product1, *product2, *product3;
    product1 = new Book("it100", 9, "The Ship", "Peter Harris");
    product2 = new DVD("it101", 4, "Flying Bird", 2016, "Janny Macwood", "Action" );
   
    // check the size 
    REQUIRE( Product::products.size() == 2 );

    // add another item and check the size
    product3 = new CD("it102", 8, "New Dawn", "pop", "Jack Will");
    REQUIRE( Product::products.size() == 3 );

    // restock should increase the n_items to 10
    product1->restock(10);
    REQUIRE(product1->getNItems() == 10);

    // should not change when enter a negative amount 
    product1->restock(-5);
    REQUIRE(product1->getNItems() == 10);

    // intially n_items is 0
    REQUIRE(product2->getNItems() == 0);


    // sell product 1
    REQUIRE( product1->sell(2) );
    // n_items = 10 - 2
    REQUIRE( product1->getNItems() == 8);
    // sales = 2    
    REQUIRE( product1->getNSales() == 2 );

    // cannot tell product2 because no stock available
    REQUIRE( !product2->sell(1) );
    REQUIRE( product2->getNSales() == 0);

    // test update stock
    product2->updateStock(5);
    REQUIRE( product2->getNItems() == 5 );


    // test remove product
    REQUIRE( Product::remove("it102") );
    REQUIRE( !Product::remove("invalid_id") );
    REQUIRE( Product::products.size() == 2);    // no of items must be 2

    // save and clear
    Product::save();    
    Product::clear();   

    // load
    Product::load();
    REQUIRE( Product::products.size() == 2);

    // check loded product 
    product1 = Product::get("it100");
    REQUIRE( product1 != (Product*)0);
    REQUIRE( product1->price == 9);
}
