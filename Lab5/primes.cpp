#include <iostream>

using namespace std;

int main()
{
  int num;

  cout << "Enter an integer: ";
  cin >> num;

  cout << "Primes to " << num << " are: ";

  for (int i = 2; i <= num; i++){
    bool isPrime = true;
    for(int j = 2; j <= i / 2; j++){
      if (i % j == 0) isPrime = false;
    }
    if (isPrime) cout << i << " ";
  }
  cout << endl;
}
  
